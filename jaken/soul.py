import asyncio
import socketio

class PatchedAsyncClient(socketio.AsyncClient):
    async def _handle_event(self, namespace, id, data):
        namespace = namespace or '/'
        await super()._handle_event(namespace, id, data)
        await self._trigger_event('message', namespace, *data[1:])

class SoulClient:
    def __init__(self):
        self.sio = PatchedAsyncClient(logger=True)
        self.sio.on('connect', self._on_connect)
        self.sio.on('message', self._on_message)

        self.call_lock = asyncio.Lock()
        self.callback = None

    async def _on_connect(self, namespace=None):
        await self.call('init')

    async def _on_message(self, msg):
        if self.call_lock.locked():
            self.call_lock.release()
        if self.callback:
            self.callback.set_result(msg)

    async def connect(self):
        await self.sio.connect("wss://animesoul.com/socket.io/", transports=['websocket'])

    async def run(self):
        await self.sio.wait()

    async def cast(self, event_name, data=None):
        await self.sio.emit(event_name, data)

    async def call(self, event_name, data=None):
        await self.call_lock.acquire()
        await self.sio.emit(event_name, data)

        self.callback = asyncio.get_event_loop().create_future()
        return await self.callback

class Card:
    IMAGE_CDN = "https://cdn.animesoul.com/images/cards"

    def __init__(self, kwargs):
        self.pk = kwargs.get("_id")
        self.name = kwargs.get("name")
        self.slug = kwargs.get("slug")
        self.tier = kwargs.get("tier")
        self.claim_count = kwargs.get("claim_count")

        self.anime = None
        self.batch = None
        try:
            extra = kwargs.get("category")

            self.anime = extra[0]
            self.batch = extra[-1]
        except IndexError:
            pass

        self.filename = kwargs.get("file")
        self.url = f"{self.IMAGE_CDN}/{self.tier}/{self.filename}"

    def to_dict(self):
        return {
            "id": self.pk,
            "name": self.name,
            "tier": self.tier,
            "anime": self.anime,
            "link": self.url,
            "claim_count": self.claim_count,
            "batch": self.batch,
        }
