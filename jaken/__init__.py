from jaken.rpc import SoulServer

import aiomas
import asyncio

async def main():
    soul_service = SoulServer()
    await soul_service.connect()

    root_service = aiomas.rpc.ServiceDict({
        "soul": soul_service,
    })

    rpc_server = await aiomas.rpc.start_server(('localhost', 5444), root_service)

    try:
        await soul_service.soul_client.run()
    finally:
        rpc_server.close()

if __name__ == "__main__":
    asyncio.run(main())
