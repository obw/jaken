# jaken

Python component of Kirara, responds to RPC

## Setup

```sh
python3 -m venv env
./env/bin/pip install -r requirements.txt

# running the server
./env/bin/python -m jaken
```

## RPC directory

### `soul`

Searching for cards:
```js
rpc.remote.soul.search_cards("blake").then((cards) => {
    // `cards` is an array of cards
});
```

Fetching a specific card by ID:
```js
rpc.remote.soul.get_card("5f2b3701a5a84e32a258df1b", /* with_users: */ true)
    .then((data) => {
        // `data.card` is a Card
        // if with_users is true, `data.users` is an array of users
    });
```
