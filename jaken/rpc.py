import aiomas

from jaken.soul import SoulClient, Card

class SoulServer:
    router = aiomas.rpc.Service()

    def __init__(self):
        self.soul_client = SoulClient()

    async def connect(self):
        await self.soul_client.connect()

    @router.expose
    async def search_cards(self, search_term, tier=None):
        r = await self.soul_client.call("cardindex", {"search": search_term, "category": tier})
        cards = [Card(doc).to_dict() for doc in r['data']['docs']]

        return cards

    @router.expose
    async def get_card(self, card_id, with_users=False):
        r = await self.soul_client.call("cardview", {"cardid": card_id})
        card = Card(r['card']).to_dict()
        users = r['users']

        if with_users:
            return dict(card=card, users=users)
        else:
            return dict(card=card)
